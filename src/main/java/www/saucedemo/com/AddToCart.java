package www.saucedemo.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class AddToCart {
    static WebDriver webDriver;
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver");
        webDriver=new ChromeDriver();
        webDriver.get("https://www.saucedemo.com/");
        webDriver.findElement(By.id("user-name")).sendKeys("standard_user");
        webDriver.findElement(By.id("password")).sendKeys("secret_sauce");
        webDriver.findElement(By.id("login-button")).click();
        List<Double> priceList=inventoryPrice();
        Double maxPrice= Collections.max(priceList);
        int indexOfMaxPrice=priceList.indexOf(maxPrice);
        List<WebElement> addtoCartList=webDriver.findElements(By.xpath("//*[@class='pricebar']/button"));
        addtoCartList.get(indexOfMaxPrice).click();
        //webDriver.quit();
    }

    public static List<Double> inventoryPrice(){
        List<Double> priceList=new ArrayList<Double>();
        List<WebElement> webElementList=webDriver.findElements(By.className("inventory_item_price"));
        for(int i=0;i<webElementList.size();i++){
            String price=webElementList.get(i).getText();
            String newStr = price.replace("$", "").replace(",","");
            priceList.add(Double.valueOf(newStr));
        }
        return priceList;
    }
}
